CC = gcc
ASM = nasm
STD = -std=gnu89
WARNINGS = -Wall -Wextra -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith
WARNINGS += -Wfloat-equal -Wswitch-enum -Wstrict-aliasing -Wno-missing-braces
WARNINGS += -Wno-cast-function-type -Wno-stringop-truncation #-Wno-switch-enum
WARNINGS += -Wno-unused-label
DEFINES = -D _DEFAULT_SOURCE
DEFINES += -D _GNU_SOURCE
DEFINES += -D __DEBUG__

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__

OPTIMIZE = -O2 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
#MARCH = -march=native
#MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
#CC_FLAGS += $(MARCH)
#CC_FLAGS += $(MTUNE)

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)
#CC_FLAGS_D += -fsanitize=address

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable

ASM_FLAGS = -Wall


FILES = EntryPoint.o LoaderOverrides.o TestOverrides.o CurlSensor.o MicroHttpdOverrides.o MySqlSensor.o PostgresSensor.o


all: $(FILES)
	gcc -g -shared -nostdlib -fPIC -ldl -lpthread -lonesdk_shared -o bin/libadt4c_d.so bin/*.o $(DEFINES) $(STD) $(WARNINGS)
	rm bin/*.o

example:
	gcc -g -o bin/Example -ladt4c_test Test/Example.c

testlib:
	gcc -g -shared -fPIC -o bin/libadt4c_test.so Test/TestLib.c

bin:
	mkdir -p bin/

clean:
	rm bin/*


curltest: bin 
	gcc -g -o bin/CurlTest -lcurl Test/CurlTest.c

microhttpdtest: bin
	gcc -g -o bin/MicroHttpdTest -lcurl -lmicrohttpd Test/MicroHttpdTest.c

mysqltest: bin
	gcc -g -o bin/MySqlTest -lonesdk_shared -I/usr/include/mysql -I/usr/include/mysql/mysql -L/usr/lib/ -lmariadb Test/MySqlTest.c

postgrestest: bin
	gcc -g -o bin/PostgresTest -lonesdk_shared -lpq Test/PostgresTest.c



EntryPoint.o: bin
	gcc -g -fPIC -c -o bin/EntryPoint.o Src/EntryPoint.c $(DEFINES) $(STD) $(WARNINGS)

LoaderOverrides.o: bin
	gcc -g -fPIC -c -o bin/LoaderOverrides.o Src/Loader/LoaderOverrides.c $(DEFINES) $(STD) $(WARNINGS)

TestOverrides.o: bin
	gcc -g -fPIC -c -o bin/TestOverrides.o Src/Test/TestOverrides.c $(DEFINES) $(STD) $(WARNINGS)

CurlSensor.o: bin
	gcc -g -fPIC -c -o bin/CurlSensor.o Src/OutgoingWeb/CurlSensor.c $(DEFINES) $(STD) $(WARNINGS)

MicroHttpdOverrides.o: bin
	gcc -g -fPIC -c -o bin/MicroHttpdOverrides.o Src/IncomingWeb/MicroHttpdOverrides.c $(DEFINES) $(STD) $(WARNINGS)

MySqlSensor.o: bin
	gcc -g -fPIC -c -o bin/MySqlSensor.o Src/Database/MySqlSensor.c $(DEFINES) $(STD) $(WARNINGS)

PostgresSensor.o: bin
	gcc -g -fPIC -c -o bin/PostgresSensor.o Src/Database/PostgresSensor.c $(DEFINES) $(STD) $(WARNINGS)



