#define __DEBUG__
#include <mysql/mysql.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <onesdk/onesdk.h>


int main(int argc, char **argv)
{
	MYSQL *mysql;
	MYSQL_RES *res;
	MYSQL_ROW row;
	int num_fields;
	int num_rows;
	onesdk_tracer_handle_t osdkthCustom;

	onesdk_stub_process_cmdline_args(argc, (onesdk_xchar_t const* const*)argv, 1);
	onesdk_stub_strip_sdk_cmdline_args(&argc, argv);
	onesdk_initialize_2(0);

	mysql = mysql_init(NULL); 
	mysql_real_connect(
		mysql,
		"127.0.0.1",
		"root",
		"example",
		"mysql",
		3306,
		NULL,
		0
	);

	printf("MySQL Server: %s\n", mysql_get_server_info(mysql));

	for (;;)
	{ 
		osdkthCustom = onesdk_customservicetracer_create(
			onesdk_asciistr("DatabaseRequestThread"),
			onesdk_asciistr("main()")
		);

		onesdk_tracer_start(osdkthCustom);

		mysql_query(mysql, "SELECT Host,User FROM user");

		res = mysql_store_result(mysql);
		num_fields = (int)mysql_num_fields(res);
		num_rows = (int)mysql_num_rows(res);

		#ifdef __DEBUG__
		fprintf(
			stderr,
			"Rows: %d\tFields: %d\n",
			num_rows, num_fields
		);
		#endif

		while ((row = mysql_fetch_row(res)))
		{
			int i;
			for (
				i = 0;
				i < num_fields;
				i++
			){ printf("%s ", row[i] ? row[i] : "NULL"); }

			printf("\n");
		}

		mysql_free_result(res);

		sleep(2);	
		onesdk_tracer_end(osdkthCustom);
	}

	mysql_close(mysql);
	return 0;
}
