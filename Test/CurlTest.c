#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <curl/curl.h>


int
main(
	int		argc,
	char		**argv
){
	for (;;)
	{
		CURL  *cRequest;
		struct curl_slist *cslHeaders;
		char *lpszData;
		char *lpszOut;

		cRequest = curl_easy_init();
		cslHeaders = NULL;
		lpszData = NULL;
		lpszOut = NULL;

		cslHeaders = curl_slist_append(cslHeaders, "Adt4cTest: AutoDynatraceForC");

		lpszData = malloc(8);
		memset(lpszData, 0x00, 8);
		strncpy(lpszData, "Hello", 8 - 1);

		/* curl_easy_setopt(cRequest, CURLOPT_URL, "http://icanhazip.com/"); */
		/* curl_easy_setopt(cRequest, CURLOPT_URL, "http://127.0.0.1:8080/Reproducer/apiTest_GET?debug=true");  */
		curl_easy_setopt(cRequest, CURLOPT_URL, "http://127.0.0.1:8081/"); 
		curl_easy_setopt(cRequest, CURLOPT_HTTPHEADER, cslHeaders);
		curl_easy_setopt(cRequest, CURLOPT_PRIVATE, lpszData);
		curl_easy_getinfo(cRequest, CURLINFO_PRIVATE, &lpszOut);
		printf("Private Data: %s\n", lpszOut);
		curl_easy_perform(cRequest);

		curl_easy_cleanup(cRequest);
		sleep(2);
	}

	return 0;
}