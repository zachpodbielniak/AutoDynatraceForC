#include <microhttpd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <curl/curl.h>

#define PAGE "<html><head><title>libmicrohttpd demo</title>"\
             "</head><body>libmicrohttpd demo</body></html>"

static enum MHD_Result
ahc_echo(
	void * cls,
	struct MHD_Connection * connection,
	const char * url,
	const char * method,
	const char * version,
	const char * upload_data,
	size_t * upload_data_size,
	void ** ptr
){
	struct MHD_Response *response;
	int ret;

	CURL  *cRequest;
	struct curl_slist *cslHeaders;
	printf("Called: %s\n", url);

	cRequest = curl_easy_init();
	cslHeaders = NULL;

	cslHeaders = curl_slist_append(cslHeaders, "Adt4cTest: AutoDynatraceForC");

	curl_easy_setopt(cRequest, CURLOPT_URL, "http://127.0.0.1:8080/Reproducer/apiTest_GET?debug=true"); 
	curl_easy_setopt(cRequest, CURLOPT_HTTPHEADER, cslHeaders);
	curl_easy_perform(cRequest);

	curl_easy_cleanup(cRequest);


	response = MHD_create_response_from_buffer (
		strlen("Hello World"),
		"Hello World",
		MHD_RESPMEM_PERSISTENT
	);
	
	ret = MHD_queue_response(
		connection,
		MHD_HTTP_OK,
		response
	);


	MHD_destroy_response(response);
	return ret;
}




int 
main(
	int argc,
	char ** argv
){
	struct MHD_Daemon * d;

	if (argc != 2) 
	{
		printf("%s PORT\n", argv[0]);
		return 1;
	}

	d = MHD_start_daemon(
		MHD_USE_THREAD_PER_CONNECTION,
		atoi(argv[1]),
		NULL,
		NULL,
		&ahc_echo,
		PAGE,
		MHD_OPTION_END
	);
	
	if (d == NULL)
 	{ return 1; }

 	(void) getc (stdin);
	MHD_stop_daemon(d);
	return 0;
}