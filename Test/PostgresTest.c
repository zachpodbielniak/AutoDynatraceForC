#include <libpq-fe.h>
#include <libpq-events.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <onesdk/onesdk.h>




int 
main(
	int 		argc,
	char 		**argv
){
	PGconn *pgsql; 
	PGresult *res;
	int num_rows;
	int i;
	onesdk_tracer_handle_t osdkTracer;

	onesdk_initialize_2(0);
	

	/* pgsql = PQconnectdb("user=postgres password=example dbname=test host=127.0.0.1 port=5432"); */
	pgsql = PQsetdbLogin(
		"127.0.0.1",
		"5432",
		NULL,
		NULL,
		"test",
		"postgres",
		"example"
	);

	printf("User: %s\n", PQuser(pgsql));
	printf("DB: %s\n", PQdb(pgsql));
	printf("Host: %s\n", PQhost(pgsql));

	for (;;)
	{
		osdkTracer = onesdk_customservicetracer_create(
			onesdk_utf8str("DatabaseRequestThread"),
			onesdk_utf8str("main()")
		);

		onesdk_tracer_start(osdkTracer);

		res = PQexec(pgsql, "SELECT * FROM user");
		num_rows = PQntuples(res);

		for (
			i = 0;
			i < num_rows;
			i++
		){ printf("User:\t%s\n", PQgetvalue(res, i, 0)); }

		PQclear(res);
		sleep(2);
		onesdk_tracer_end(osdkTracer);
	}


	PQfinish(pgsql);
	return 0;
}