#ifndef PODMON_LOADERORIGINAL_H
#define PODMON_LOADERORIGINAL_H


#include "../Prereqs.h"


typedef int (*LPFN_REAL_LIBCSTARTMAIN)(
	int 		(*lpfnMain)(int, char **, char **),
	int		iArgCount,
	char		**dlpszArgValues,
	void 		(*lpfnInit)(void),
	void 		(*lpfnFini)(void),
	void 		(*lpfnRtldFini)(void),
	void 		(*lpStackEnd)
);


#endif