#include "LoaderOriginal.h"




void * 
Adt4cMain(
	void	*lpData
){
	lpData = lpData; 

	for (;;)
	{
		printf("Adt4cMain()\n");
		sleep(1000);
	}

	return NULL;
}



int
Adt4cInit(
	void
){
	if (ONESDK_SUCCESS == onesdk_initialize_2(0))
	{ return 0; }

	return 1;
}




int
__libc_start_main(
	int 		(*lpfnMain)(int, char **, char **),
	int		iArgCount,
	char		**dlpszArgValues,
	void 		(*lpfnInit)(void),
	void 		(*lpfnFini)(void),
	void 		(*lpfnRtldFini)(void),
	void 		(*lpStackEnd)
){
	LPFN_REAL_LIBCSTARTMAIN lpfnRealLibcStartMain;
	pthread_t ptMain;

	lpfnRealLibcStartMain = (LPFN_REAL_LIBCSTARTMAIN)dlsym(RTLD_NEXT, "__libc_start_main");

	#ifdef __DEBUG__
	fprintf(stderr, "__libc_start_main()\n");
	#endif

	/* Init */
	if (0 == Adt4cInit())
	{ pthread_create(&ptMain, NULL, Adt4cMain, NULL); }

	return lpfnRealLibcStartMain(
		lpfnMain,
		iArgCount,
		dlpszArgValues,
		lpfnInit,
		lpfnFini,
		lpfnRtldFini,
		lpStackEnd
	);
}


