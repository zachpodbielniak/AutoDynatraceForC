#ifndef AUTODYNATRACEFORC_PREREQS_H
#define AUTODYNATRACEFORC_PREREQS_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif 

#define __NO_UNISTD

#include <dlfcn.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <pthread.h>
#include <unistd.h>
#include <onesdk/onesdk.h>


#ifdef __CODE_ENV
#include <curl/curl.h>
#include <microhttpd.h>
#endif

#endif