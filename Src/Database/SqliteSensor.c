#include "SqliteOriginal.h"




static 
int 
__Adt4cSqliteExecCallback(
	void		*lpUserData,
	int		nColumns,
	char		**dlpszValues,
	char		**dlpszColumns
){
	LPSQLITE_TRACE_INFO lpstiData;
	int iRet;

	lpstiData = lpUserData;
	iRet = lpstiData->lpfnOriginal(
		lpstiData->lpOriginalArg,
		nColumns,
		dlpszValues,
		dlpszColumns
	);

	lpstiData->iRowCount++;
	return iRet;
}





#ifdef sqlite3_exec
#undef sqlite3_exec
#endif
int
sqlite3_exec(
	sqlite3		*db,
	const char	*sql,
	int 		(*callback)(void *, int, char **, char **),
	void		*arg,
	char		**errmsg
){
	LPFN_SQLITE_EXEC_PROC lpfnExec;
	SQLITE_TRACE_INFO stiData;
	int iRet;

	lpfnExec = dlsym(RTLD_NEXT, "sqlite3_exec");
	memset(&stiData, 0x00, sizeof(SQLITE_TRACE_INFO));

	stiData.lpfnOriginal = callback;
	stiData.lpOriginalArg = arg;

	stiData.osdkdihDbInfo = onesdk_databaseinfo_create(
		onesdk_utf8str("sqlite"),
		onesdk_utf8str(ONESDK_DATABASE_VENDOR_SQLITE),
		ONESDK_CHANNEL_TYPE_IN_PROCESS,
		onesdk_utf8str("sqlite")
	);

	stiData.osdkTracer = onesdk_databaserequesttracer_create_sql(
		stiData.osdkdihDbInfo,
		onesdk_utf8str(sql)
	);

	onesdk_tracer_start(stiData.osdkTracer);


	/* Do what they came here to do */
	iRet = lpfnExec(
		db,
		sql,
		__Adt4cRuntimeDebug,
		&stiData,
		errmsg
	);


	onesdk_databaserequesttracer_set_returned_row_count(
		stiData.osdkTracer,
		stiData.iRowCount
	);

	onesdk_tracer_end(stiData.osdkTracer);
	onesdk_databaseinfo_delete(stiData.osdkdihDbInfo);

	return iRet;
}