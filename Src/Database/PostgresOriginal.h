#include <libpq-fe.h>
#include <libpq-events.h>
#include "../Prereqs.h"


typedef PGconn *(*LPFN_PQ_SET_DB_LOGIN_PROC)(
	const char 	*pghost,
	const char 	*pgport,
	const char	*pgoptions,
	const char	*pgtty,
	const char	*dbname,
	const char	*login,
	const char	*pwd
);




typedef PGresult *(*LPFN_PG_EXEC_PROC)(
	PGconn		*conn,
	const char	*query
);





typedef struct __POSTGRES_TRACE_INFO
{
	onesdk_databaseinfo_handle_t		osdkdihDbInfo;
	onesdk_tracer_handle_t			osdkTracer;
	PGresult				*res;
	PGconn					*pgsql;
} POSTGRES_TRACE_INFO, *LPPOSTGRES_TRACE_INFO;