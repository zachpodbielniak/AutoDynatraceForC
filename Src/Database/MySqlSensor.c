#include "MySqlOriginal.h"



#define SET_MYSQL_DATA(M, D)		\
	(M)->unused_6 = D


#define GET_MYSQL_DATA(M)		\
	(M->unused_6)


/* -- Not actually needed with current implementation
#define SET_MYSQL_DATA_FOR_RES(R, D)	\
	(R)->data->extension = D


#define GET_MYSQL_DATA_FROM_RES(R)	\
	(((MYSQL *)((R)->data->extension))->unused_6)
*/




/* -- Not actually needed with current implementation
static 
long long 
__Adt4cCalculateFetchCountMySql(
	LPMYSQL_TRACE_INFO	lpmtiData,
	MYSQL_RES		*res
){
	MYSQL_ROWS *iterator;
	long long llRet;
	if (NULL == lpmtiData || NULL == res)
	{ return -1; }

	llRet = 0;

	if (NULL == lpmtiData->mresResult->data_cursor)
	{ return lpmtiData->myullRows; }

	for (
		llRet = lpmtiData->myullRows, iterator = lpmtiData->mresResult->data_cursor;
		llRet >= 0 && lpmtiData->mresResult->data_cursor;
		llRet--, iterator = iterator->next
	){ (void)0; }

	if (llRet < 0)
	{ llRet = 0; }

	return llRet;
}
*/



static 
void 
__Adt4cDetermineIfMySqlOrMaria(
	LPMYSQL_TRACE_INFO	lpmtiData
){
	LPFN_MYSQL_QUERY_PROC lpfnQuery;
	LPFN_MYSQL_STORE_RESULT_PROC lpfnStore;

	if (NULL == lpmtiData)
	{ return; }

	if (strstr(mysql_get_server_info(lpmtiData->mysql), "MariaDB"));
	{ lpmtiData->osdkbMaria = 1; }

	return;
}



#ifdef mysql_init
#undef mysql_init
#endif
MYSQL *
mysql_init(
	MYSQL		*data
){
	MYSQL *ret;
	LPFN_MYSQL_INIT_PROC lpfnMysqlInit;
	LPMYSQL_TRACE_INFO lpmtiData;

	lpfnMysqlInit = (LPFN_MYSQL_INIT_PROC)dlsym(RTLD_NEXT, "mysql_init");
	lpmtiData = malloc(sizeof(MYSQL_TRACE_INFO));
	memset(lpmtiData, 0x00, sizeof(MYSQL_TRACE_INFO));

	if (NULL == lpfnMysqlInit)
	{ return NULL; }

	if (NULL == data)
	{
		ret = lpfnMysqlInit(data);
		ret->unused_6 = lpmtiData;
		lpmtiData->mysql = ret;
	}
	else 
	{
		ret = lpfnMysqlInit(data);
		data->unused_6 = lpmtiData;
		lpmtiData->mysql = data;
	}

	#ifdef __DEBUG__
	fprintf(stderr, "mysql_init()\n");
	#endif

	return ret;
}




#ifdef mysql_real_connect
#undef mysql_real_connect
#endif
MYSQL *
mysql_real_connect(
	MYSQL		*mysql,
	const char	*host,
	const char	*user,
	const char	*passwd,
	const char	*db,
	unsigned int	port,
	const char	*unix_socket,
	unsigned long	clientflag
){
	LPMYSQL_TRACE_INFO lpmtiData;
	LPFN_MYSQL_REAL_CONNECT_PROC lpfnConnect;
	MYSQL *ret;
	char csEndpoint[0x100];

	memset(csEndpoint, 0x00, sizeof(csEndpoint));
	lpmtiData = GET_MYSQL_DATA(mysql);
	ret = NULL;
	
	lpfnConnect = dlsym(RTLD_NEXT, "mysql_real_connect");
	
	if (NULL != lpfnConnect)
	{ 
		ret = lpfnConnect(
			mysql,
			host,
			user,
			passwd,
			db,
			port,
			unix_socket,
			clientflag
		);
	}
	
	SET_MYSQL_DATA(mysql, lpmtiData);

	if (ONESDK_INVALID_HANDLE != lpmtiData->osdkdihDbInfo)
	{
		onesdk_databaseinfo_delete(lpmtiData->osdkdihDbInfo);
		lpmtiData->osdkdihDbInfo = ONESDK_INVALID_HANDLE;
	}

	if (db)
	{ 
		snprintf(
			csEndpoint,
			sizeof(csEndpoint) - 1,
			"%s:%d",
			host,
			port
		);
	}
	else 
	{
		snprintf(
			csEndpoint,
			sizeof(csEndpoint) - 1,
			"%s",
			unix_socket
		);
	}

	__Adt4cDetermineIfMySqlOrMaria(lpmtiData);

	lpmtiData->osdkdihDbInfo = onesdk_databaseinfo_create(
		onesdk_utf8str(db),
		(lpmtiData->osdkbMaria) ? onesdk_utf8str(ONESDK_DATABASE_VENDOR_MARIADB) : onesdk_utf8str(ONESDK_DATABASE_VENDOR_MYSQL),
		(NULL != host) ? ONESDK_CHANNEL_TYPE_TCP_IP : ONESDK_CHANNEL_TYPE_UNIX_DOMAIN_SOCKET,
		onesdk_utf8str(csEndpoint)
	);

	lpmtiData->lpszDbName = strdup(db);
	lpmtiData->lpszEndpoint = strdup(csEndpoint);
	lpmtiData->lpszVendor = ONESDK_DATABASE_VENDOR_MYSQL;


	return ret;
}




#ifdef mysql_store_result
#undef mysql_store_result
#endif 
MYSQL_RES *
mysql_store_result(
	MYSQL		*mysql
){
	LPMYSQL_TRACE_INFO lpmtiData;
	LPFN_MYSQL_STORE_RESULT_PROC lpfnStore;

	if (NULL == mysql)
	{ return NULL; }

	lpmtiData = GET_MYSQL_DATA(mysql);
	if (NULL == lpmtiData)
	{ return NULL; }

	/* Just return the local copy */
	if (NULL != lpmtiData->mresResult)
	{ return lpmtiData->mresResult; }

	lpfnStore = dlsym(RTLD_NEXT, "mysql_store_result");
	if (NULL != lpfnStore)
	{ lpmtiData->mresResult = lpfnStore(mysql); }

	/* -- Not needed for current implementation
	SET_MYSQL_DATA_FOR_RES(lpmtiData->mresResult, mysql);
	*/

	return lpmtiData->mresResult;
}



/* -- Not actually needed with current implementation
#ifdef mysql_free_result
#undef mysql_free_result
#endif 
void
mysql_free_result(
	MYSQL_RES	*res
){
	LPMYSQL_TRACE_INFO lpmtiData;
	LPFN_MYSQL_STORE_RESULT_PROC lpfnStore;
	onesdk_int32_t iFetchCount;

	if (NULL == res)
	{ return NULL; }

	lpmtiData = GET_MYSQL_DATA_FROM_RES(res);

	lpfnStore = dlsym(RTLD_NEXT, "mysql_free_result");
	if (NULL != lpfnStore)
	{ lpfnStore(res); }

	if (NULL != lpmtiData->osdkdihDbInfo)
	{
		iFetchCount = __Adt4cCalculateFetchCountMySql(lpmtiData, res);
		onesdk_databaserequesttracer_set_round_trip_count(
			lpmtiData->osdkthDbRequest,
			iFetchCount
		);

		onesdk_tracer_end(lpmtiData->osdkthDbRequest);
		lpmtiData->osdkthDbRequest = ONESDK_INVALID_HANDLE;

		#ifdef __DEBUG__
		fprintf(
			stderr,
			"mysql_free_result(%p) - Row Count: %d\n",
			res,
			(int)lpmtiData->myullRows,
		);
		#endif
	}

	lpmtiData->mresResult = NULL;
	lpmtiData->myullRows = 0;

	return;
}
*/



/* -- Not actually needed with current implementation
#ifdef mysql_num_rows
#undef mysql_num_rows
#endif 
my_ulonglong 
mysql_num_rows(
	MYSQL_RES	*res
){
	LPMYSQL_TRACE_INFO lpmtiData;
	LPFN_MYSQL_NUM_ROWS_PROC lpfnRows;

	lpfnRows = dlsym(RTLD_NEXT, "mysql_num_rows");

	if (NULL == lpfnRows)
	{ return 0; }

	if (NULL == res)
	{ return lpfnRows(res); }

	lpmtiData = GET_MYSQL_DATA(res->handle);
	if (NULL == lpmtiData)
	{ return lpfnRows(res); }

	if (0 == lpmtiData->myullRows)
	{ lpmtiData->myullRows = lpfnRows(res);}

	return lpmtiData->myullRows;
}
*/




#ifdef mysql_real_query
#undef mysql_real_query
#endif 
int
mysql_real_query(
	MYSQL		*mysql,
	const char	*stmt_str,
	unsigned long	length
){
	LPMYSQL_TRACE_INFO lpmtiData;
	LPFN_MYSQL_REAL_QUERY_PROC lpfnQuery;
	int iRet;
	onesdk_tracer_handle_t osdkTracer;

	iRet = 1;

	if (NULL == mysql || NULL == stmt_str || 0 == length)
	{ return 1; }

	lpmtiData = GET_MYSQL_DATA(mysql);
	if (NULL != lpmtiData) /* Do the tracing */
	{
		lpmtiData->mresResult = NULL;
		lpmtiData->myullRows = 0;

		osdkTracer = onesdk_databaserequesttracer_create_sql(
			lpmtiData->osdkdihDbInfo,
			onesdk_utf8str(stmt_str)
		);

		onesdk_tracer_start(osdkTracer);
	}

	lpfnQuery = dlsym(RTLD_NEXT, "mysql_real_query");
	if (NULL != lpfnQuery)
	{ iRet = lpfnQuery(mysql, stmt_str, length); }


	if (NULL != lpmtiData) /* End the tracing */
	{
		mysql_store_result(mysql);
		lpmtiData->myullRows = mysql_num_rows(lpmtiData->mresResult);

		onesdk_databaserequesttracer_set_returned_row_count(
			osdkTracer,
			(onesdk_int32_t)lpmtiData->myullRows
		);

		onesdk_tracer_end(osdkTracer);
	}

	return iRet;
}




#ifdef mysql_query
#undef mysql_query
#endif 
int
mysql_query(
	MYSQL		*mysql,
	const char	*q
){
	/* 
	According to https://dev.mysql.com/doc/c-api/8.0/en/mysql-real-query.html, 
	mysql_query() is just mysql_real_query() with strlen, and accepts only a string,
	not raw byte data
	*/

	return mysql_real_query(
		mysql,
		q,
		strlen(q)
	);
}



