#include "PostgresOriginal.h"


static
int
__Adt4cPostgresEventProc(
	PGEventId		evtId,
	void			*lpInfo,
	void			*lpUserData
){
	LPPOSTGRES_TRACE_INFO lpttiData;

	lpttiData = lpUserData;

	switch(evtId)
	{
		case PGEVT_REGISTER:
		{ break; }

		case PGEVT_CONNRESET:
		{ break; }

		case PGEVT_CONNDESTROY:
		{
			/* TODO: Cleanup Code */
			break;
		}

		case PGEVT_RESULTCREATE:
		{
			PGEventResultCreate *e = lpInfo;
			lpttiData->res = e->result;
			PQresultSetInstanceData(e->result, __Adt4cPostgresEventProc, lpUserData);

			/* If result set has tuples, lets get the returned count */
			if (PGRES_TUPLES_OK == PQresultStatus(e->result))
			{
				onesdk_databaserequesttracer_set_returned_row_count(
					lpttiData->osdkTracer,
					(onesdk_int32_t)PQntuples(e->result)
				);
			}

			break;
		}

		case PGEVT_RESULTCOPY:
		{ break; }

		case PGEVT_RESULTDESTROY:
		{
			PGEventResultDestroy *e = lpInfo;

			onesdk_tracer_end(lpttiData->osdkTracer);
			lpttiData->osdkTracer = ONESDK_INVALID_HANDLE;

			break;
		}

		default: break;
	}

	return 1;
}



#ifdef connectDBComplete
#undef connectDBComplete
#endif
int
connectDBComplete(
	PGconn		*conn
){
	if (NULL == conn)
	{ return -1; }

	int (*lpfnComplete)(PGconn *);

	lpfnComplete = dlsym(RTLD_NEXT, "connectDBComplete");

	printf("calling it\n");
	return lpfnComplete(conn);
}


#ifdef PQsetdbLogin
#undef PQsetdbLogin
#endif
PGconn *
PQsetdbLogin(
	const char 	*pghost,
	const char 	*pgport,
	const char	*pgoptions,
	const char	*pgtty,
	const char	*dbname,
	const char	*login,
	const char	*pwd
){
	LPFN_PQ_SET_DB_LOGIN_PROC lpfnLogin;
	LPPOSTGRES_TRACE_INFO lpptiData;
	PGconn *pgsql;
	char csEndpoint[256];

	lpfnLogin = dlsym(RTLD_NEXT, "PQsetdbLogin");
	pgsql = NULL;
	memset(csEndpoint, 0x00, sizeof(csEndpoint));

	if (NULL != lpfnLogin)
	{
		pgsql = lpfnLogin(
			pghost,
			pgport,
			pgoptions,
			pgtty,
			dbname,
			login,
			pwd
		);
	}

	/* Create database info, and set instance data to our struct */
	if (NULL != pgsql)
	{
		lpptiData = malloc(sizeof(POSTGRES_TRACE_INFO));
		memset(lpptiData, 0x00, sizeof(POSTGRES_TRACE_INFO));

		snprintf(
			csEndpoint,
			sizeof(csEndpoint) - 1,
			"%s:%s",
			pghost, pgport
		);

		lpptiData->osdkdihDbInfo = onesdk_databaseinfo_create(
			onesdk_utf8str(dbname),
			onesdk_utf8str(ONESDK_DATABASE_VENDOR_POSTGRESQL),
			ONESDK_CHANNEL_TYPE_TCP_IP,
			onesdk_utf8str(csEndpoint)
		);

		lpptiData->pgsql = pgsql;
		PQregisterEventProc(pgsql, __Adt4cPostgresEventProc, "__Adt4cPostgresEventProc", lpptiData);
		PQsetInstanceData(pgsql, __Adt4cPostgresEventProc, lpptiData);
	}

	return pgsql;
}




#ifdef PQexec
#undef PQexec
#endif
PGresult *
PQexec(
	PGconn		*conn,
	const char	*query
){
	LPFN_PG_EXEC_PROC lpfnExec;
	LPPOSTGRES_TRACE_INFO lpttiData;

	lpfnExec = dlsym(RTLD_NEXT, "PQexec");

	lpttiData = PQinstanceData(conn, __Adt4cPostgresEventProc);
	lpttiData->osdkTracer = onesdk_databaserequesttracer_create_sql(
		lpttiData->osdkdihDbInfo,
		onesdk_utf8str(query)
	);

	onesdk_tracer_start(lpttiData->osdkTracer);

	return lpfnExec(
		conn,
		query
	);
}