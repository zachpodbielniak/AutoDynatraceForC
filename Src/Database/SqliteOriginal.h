#ifndef ADT4C_SQLITEORIGINAL_H
#define ADT4C_SQLITEORIGINAL_H


#include "../Prereqs.h"
#include "../Runtime.h"
#include <sqlite3.h>



typedef int (*LPFN_SQLITE_EXEC_PROC)(
	sqlite3		*db,
	const char	*sql,
	int 		(*callback)(void *, int, char **, char **),
	void		*arg,
	char		**errmsg
);



typedef struct __SQLITE_TRACE_INFO
{
	onesdk_databaseinfo_handle_t		osdkdihDbInfo;
	onesdk_tracer_handle_t			osdkTracer;
	sqlite3					*db;
	int 					(*lpfnOriginal)(void *, int, char **, char **);
	void					*lpOriginalArg;	
	onesdk_int32_t				iRowCount;
} SQLITE_TRACE_INFO, *LPSQLITE_TRACE_INFO;


#endif