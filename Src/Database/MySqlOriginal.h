#include <mysql/mysql.h>
#include "../Prereqs.h"



typedef MYSQL *(*LPFN_MYSQL_INIT_PROC)(
	MYSQL		*data
);




typedef int (*LPFN_MYSQL_QUERY_PROC)(
	MYSQL		*mysql,
	const char	*q
);




typedef int (*LPFN_MYSQL_REAL_QUERY_PROC)(
	MYSQL		*mysql,
	const char	*stmt_str,
	unsigned long	length
);




typedef MYSQL_RES *(*LPFN_MYSQL_STORE_RESULT_PROC)(
	MYSQL		*mysql
);




typedef void (*LPFN_MYSQL_FREE_RESULT_PROC)(
	MYSQL		*mysql
);




typedef int (*LPFN_MYSQL_NUM_FIELDS_PROC)(
	MYSQL_RES	*res
);




typedef my_ulonglong (*LPFN_MYSQL_NUM_ROWS_PROC)(
	MYSQL_RES	*res
);



typedef MYSQL *(*LPFN_MYSQL_REAL_CONNECT_PROC)(
	MYSQL		*mysql,
	const char	*host,
	const char	*user,
	const char	*passwd,
	const char	*db,
	unsigned int	port,
	const char	*unix_socket,
	unsigned long	clientflag
);




typedef struct __MYSQL_TRACE_INFO
{
	onesdk_databaseinfo_handle_t		osdkdihDbInfo;
	char					*lpszDbName;
	char					*lpszVendor;
	char					*lpszEndpoint;
	MYSQL					*mysql;
	MYSQL_RES				*mresResult;
	my_ulonglong				myullRows;
	onesdk_bool_t				osdkbMaria;
} MYSQL_TRACE_INFO, *LPMYSQL_TRACE_INFO;