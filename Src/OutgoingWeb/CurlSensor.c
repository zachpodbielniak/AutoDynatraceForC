#include "CurlOriginal.h"
#include "../Runtime.h"



size_t 
__Adt4cCurlHeaderCallBack(
	char 			*lpszBuffer,
	size_t 			szSize,
	size_t 			szItems,
	void			*lpUserData
){
	LPCURL_PRIVATE_INFO lpcpiData;
	char csHeader[0x400];
	char *lpszValue;

	if (NULL == strstr(lpszBuffer, ":"))
	{ return szItems; }

	lpcpiData = lpUserData;
	
	if (__Adt4cRuntimeDebug)
	{ fprintf(stderr, "ResponseHeader (%p) => %s", lpcpiData->lpCurl, lpszBuffer); }

	memset(csHeader, 0x00, sizeof(csHeader));
	strncpy(csHeader, lpszBuffer, sizeof(csHeader) - 1);
	lpszValue = strstr(csHeader, ":");
	*lpszValue = '\0';
	lpszValue = (char *)(void *)((unsigned long)lpszValue + 1);
	while (' ' == *lpszValue)
	{ lpszValue = (char *)(void *)((unsigned long)lpszValue + 1); }

	onesdk_outgoingwebrequesttracer_add_response_header(
		lpcpiData->osdkhCurl,
		onesdk_asciistr(csHeader),
		onesdk_asciistr(lpszValue)
	);

	if (NULL != lpcpiData->lpfnHeaderCallBack)
	{
		return lpcpiData->lpfnHeaderCallBack(
			lpszBuffer,
			szSize,
			szItems,
			lpcpiData->lpHeaderUserData
		);
	}
	
	return szItems;
}




#ifdef curl_easy_setopt
#undef curl_easy_setopt
#endif
CURLcode 
curl_easy_setopt(
	CURL		*lpCurl, 
	CURLoption 	coOption, 
	...
){
	va_list vlData;
	LPFN_REAL_CURLEASYSETOPT lpfnCurlEasySetOpt;
	LPFN_REAL_CURLEASYGETINFO lpfnCurlEasyGetInfo;
	LPCURL_PRIVATE_INFO lpcpiData;
	CURLcode ccRet;

	lpcpiData = NULL;

	lpfnCurlEasySetOpt = (LPFN_REAL_CURLEASYSETOPT)dlsym(RTLD_NEXT, "curl_easy_setopt");
	lpfnCurlEasyGetInfo = (LPFN_REAL_CURLEASYGETINFO)dlsym(RTLD_NEXT, "curl_easy_getinfo");

	lpfnCurlEasyGetInfo(lpCurl, CURLINFO_PRIVATE, &lpcpiData);
	
	if (__Adt4cRuntimeDebug)
	{ fprintf(stderr, "curl_easy_setopt() override: %p / %p\n", lpCurl, lpcpiData->lpCurl); } 
	

	if(CURLOPT_URL == coOption)
	{
		char *lpszUrl;
			
		if (__Adt4cRuntimeDebug)
		{ fprintf(stderr, "curl_easy_setopt() CURLOPT_URL\n"); }

		va_start(vlData, coOption);
		lpszUrl = va_arg(vlData, char *);
		va_end(vlData);

		
		if (NULL != lpcpiData->lpszUrl)
		{ free(lpcpiData->lpszUrl); }

		lpcpiData->lpszUrl = strdup(lpszUrl);
			
		if (__Adt4cRuntimeDebug)
		{ fprintf(stderr, "Curl URL: %s\n", lpcpiData->lpszUrl); }

		ccRet = lpfnCurlEasySetOpt(lpCurl, coOption, lpcpiData->lpszUrl);

		return ccRet;
	}
	else if (CURLOPT_HTTPHEADER == coOption)
	{
		if (__Adt4cRuntimeDebug)
		{ fprintf(stderr, "curl_easy_setopt() CURLOPT_HTTPHEADER\n"); }

		va_start(vlData, coOption);
		lpcpiData->cslHeaders = va_arg(vlData, struct curl_slist *);
		va_end(vlData);

		if (__Adt4cRuntimeDebug)
		{ 
			if (NULL != lpcpiData->cslHeaders && NULL != lpcpiData->cslHeaders->data)
			{ fprintf(stderr, "Picked up headers, first: %s\n", lpcpiData->cslHeaders->data); }
		}

		ccRet = lpfnCurlEasySetOpt(lpCurl, coOption, lpcpiData->cslHeaders);
	}
	else if (CURLOPT_HEADERFUNCTION == coOption)
	{
		va_start(vlData, coOption);
		lpcpiData->lpfnHeaderCallBack = va_arg(vlData, LPFN_REAL_CURLHEADERFUNCTIONCALLBACK);
		va_end(vlData);
		
		if (__Adt4cRuntimeDebug)
		{ fprintf(stderr, "curl_easy_setopt() CURLOPT_HEADERFUNCTION : %p\n", lpcpiData->lpfnHeaderCallBack); }

		return CURLE_OK;
	}
	else if (CURLOPT_HEADERDATA == coOption)
	{
		va_start(vlData, coOption);
		lpcpiData->lpHeaderUserData = va_arg(vlData, void *);
		va_end(vlData);

		if (__Adt4cRuntimeDebug)
		{ fprintf(stderr, "curl_easy_setopt() CURLOPT_HEADERDATA : %p\n", lpcpiData->lpHeaderUserData); }
		
		return CURLE_OK;
	}
	else if (CURLOPT_PRIVATE == coOption)
	{
		va_start(vlData, coOption);
		lpcpiData->lpTruePrivateInfo = va_arg(vlData, void *);
		va_end(vlData);

		if (__Adt4cRuntimeDebug)
		{ fprintf(stderr, "curl_easy_setopt() CURLOPT_PRIVATE : %p\n", lpcpiData->lpTruePrivateInfo); }

		return CURLE_OK;
	}
		
	va_start(vlData, coOption);
	ccRet = lpfnCurlEasySetOpt(lpCurl, coOption, va_arg(vlData, void *));
	va_end(vlData);
	return ccRet;
}




#ifdef curl_easy_getinfo
#undef curl_easy_getinfo
#endif 
CURLcode 
curl_easy_getinfo(
	CURL 		*lpCurl,
	CURLINFO	ciInfo,
	...
){
	va_list vlData;
	LPFN_REAL_CURLEASYGETINFO lpfnCurlEasyGetInfo;
	LPCURL_PRIVATE_INFO lpcpiData;
	CURLcode ccRet;
	void **dlpOut;

	lpcpiData = NULL;

	lpfnCurlEasyGetInfo = (LPFN_REAL_CURLEASYGETINFO)dlsym(RTLD_NEXT, "curl_easy_getinfo");
	lpfnCurlEasyGetInfo(lpCurl, CURLINFO_PRIVATE, &lpcpiData);

	if (__Adt4cRuntimeDebug)
	{ fprintf(stderr, "curl_easy_getinfo() override: %p / %p\n", lpCurl, lpcpiData->lpCurl); }
	
	if (CURLINFO_PRIVATE == ciInfo)
	{
		va_start(vlData, ciInfo);
		dlpOut = va_arg(vlData, void **);
		va_end(vlData);

		if (NULL != dlpOut)
		{ *dlpOut = lpcpiData->lpTruePrivateInfo; }

		if (__Adt4cRuntimeDebug)
		{ fprintf(stderr, "curl_easy_getinfo() CURLINFO_PRIVATE : %p\n", dlpOut); }

		return CURLE_OK;
	}

	va_start(vlData, ciInfo);
	ccRet = lpfnCurlEasyGetInfo(lpCurl, ciInfo, va_arg(vlData, void *));
	va_end(vlData);
	return ccRet;
}




#ifdef curl_easy_init
#undef curl_easy_init
#endif
CURL *
curl_easy_init(
	void
){
	LPFN_REAL_CURLEASYINIT lpfnCurlEasyInit;
	LPFN_REAL_CURLEASYSETOPT lpfnCurlEasySetOpt;
	CURL *lpcRet;
	LPCURL_PRIVATE_INFO lpcpiData;

	lpfnCurlEasyInit = (LPFN_REAL_CURLEASYINIT)dlsym(RTLD_NEXT, "curl_easy_init");
	lpfnCurlEasySetOpt = (LPFN_REAL_CURLEASYSETOPT)dlsym(RTLD_NEXT, "curl_easy_setopt");
	
	if (__Adt4cRuntimeDebug)
	{ fprintf(stderr, "curl_easy_init() wrapped\n"); }

	lpcRet = lpfnCurlEasyInit();

	lpcpiData = malloc(sizeof(CURL_PRIVATE_INFO));
	memset(lpcpiData, 0x00, sizeof(CURL_PRIVATE_INFO));
	lpcpiData->lpCurl = lpcRet;

	lpfnCurlEasySetOpt(lpcRet, CURLOPT_PRIVATE, lpcpiData);
	lpfnCurlEasySetOpt(lpcRet, CURLOPT_HEADERFUNCTION, __Adt4cCurlHeaderCallBack);
	lpfnCurlEasySetOpt(lpcRet, CURLOPT_HEADERDATA, lpcpiData);

	return lpcRet;
}




#ifdef curl_easy_perform
#undef curl_easy_perform
#endif
CURLcode
curl_easy_perform(
	CURL 		*lpCurl
){
	LPFN_REAL_CURLEASYPERFORM lpfnCurlEasyPeform;
	LPFN_REAL_CURLEASYGETINFO lpfnCurlEasyGetInfo;
	LPFN_REAL_CURLSLISTAPPEND lpfnCurlSlistAppend;
	LPCURL_PRIVATE_INFO lpcpiData;
	CURLcode ccRet;
	long lResponse;
	onesdk_handle_t osdkhCurl;
	char csHeader[0x400];
	char *lpszValue;
	struct curl_slist *cslIterator;
	char *lpszMethod;
	onesdk_size_t osdksTraceTagSize;
	char *lpszTraceTag;
	char csTraceHeader[0x200];

	lpfnCurlEasyPeform = (LPFN_REAL_CURLEASYPERFORM)dlsym(RTLD_NEXT, "curl_easy_perform");
	lpfnCurlEasyGetInfo = (LPFN_REAL_CURLEASYGETINFO)dlsym(RTLD_NEXT, "curl_easy_getinfo");
	lpfnCurlSlistAppend = (LPFN_REAL_CURLSLISTAPPEND)dlsym(RTLD_NEXT, "curl_slist_append");

	lpfnCurlEasyGetInfo(lpCurl, CURLINFO_PRIVATE, &lpcpiData);
	lpfnCurlEasyGetInfo(lpCurl, CURLINFO_EFFECTIVE_METHOD, &lpszMethod);
	
	if (__Adt4cRuntimeDebug)
	{ fprintf(stderr, "curl_easy_perform() : %p / %p\n", lpCurl, lpcpiData->lpCurl); }


	osdkhCurl = onesdk_outgoingwebrequesttracer_create(
		onesdk_asciistr(lpcpiData->lpszUrl),
		onesdk_asciistr(lpszMethod)
	);
	lpcpiData->osdkhCurl = osdkhCurl;


	/* Add Headers */
	
	if (NULL != lpcpiData->cslHeaders)
	{	
		for (
			cslIterator = lpcpiData->cslHeaders;
			NULL != cslIterator;
			cslIterator = cslIterator->next
		){	
			if (NULL != cslIterator->data)
			{
				memset(csHeader, 0x00, sizeof(csHeader));
				strncpy(csHeader, cslIterator->data, sizeof(csHeader) - 1);
				lpszValue = strstr(csHeader, ":");
				*lpszValue = '\0';
				lpszValue = (char *)(void *)((unsigned long)lpszValue + 1);
				while (' ' == *lpszValue)
				{ lpszValue = (char *)(void *)((unsigned long)lpszValue + 1); }

				onesdk_outgoingwebrequesttracer_add_request_header(
					osdkhCurl,
					onesdk_asciistr(csHeader),
					onesdk_asciistr(lpszValue)
				);
			}
		}
	}
	
	/* Start the OutgoingWebRequestTracer */
	onesdk_tracer_start(osdkhCurl);

	/* Add X-Dynatrace Header*/
	onesdk_tracer_get_outgoing_dynatrace_string_tag(
		osdkhCurl,
		NULL,
		0,
		&osdksTraceTagSize
	);

	/* Get Size and the Tag for X-Dynatrace */
	if (0 != osdksTraceTagSize)
	{
		lpszTraceTag = malloc(osdksTraceTagSize);
		if (NULL != lpszTraceTag)
		{
			osdksTraceTagSize = onesdk_tracer_get_outgoing_dynatrace_string_tag(
				osdkhCurl, 
				lpszTraceTag,
				osdksTraceTagSize,
				NULL
			);
		}
	}

	/* Split out to be friendly for curl_slist */
	memset(csTraceHeader, 0x00, sizeof(csTraceHeader));
	snprintf(
		csTraceHeader,
		sizeof(csTraceHeader) - 1,
		"%s: %s",
		ONESDK_DYNATRACE_HTTP_HEADER_NAME,
		lpszTraceTag
	);
	
	/* Its in the buffer, so free the malloc'd region */
	free(lpszTraceTag);

	/* Append, and abuse the curl_easy_setopt we have */
	lpcpiData->cslHeaders = lpfnCurlSlistAppend(lpcpiData->cslHeaders, csTraceHeader);
	curl_easy_setopt(lpCurl, CURLOPT_HTTPHEADER, lpcpiData->cslHeaders);

	/* Actually do the thing they came here to  do */
	ccRet = lpfnCurlEasyPeform(lpCurl);

	/* Response code */
	lpfnCurlEasyGetInfo(lpCurl, CURLINFO_RESPONSE_CODE, &lResponse);
	onesdk_outgoingwebrequesttracer_set_status_code(
		osdkhCurl,
		(onesdk_int32_t)lResponse
	);

	onesdk_tracer_end(osdkhCurl);

	return ccRet;
}




#ifdef curl_easy_cleanup
#undef curl_easy_cleanup
#endif
void
curl_easy_cleanup(
	CURL 		*lpCurl
){
	LPFN_REAL_CURLEASYCLEANUP lpfnCurlEasyCleanUp;
	LPFN_REAL_CURLEASYGETINFO lpfnCurlEasyGetInfo;
	LPCURL_PRIVATE_INFO lpcpiData;

	lpfnCurlEasyCleanUp = (LPFN_REAL_CURLEASYCLEANUP)dlsym(RTLD_NEXT, "curl_easy_cleanup");
	lpfnCurlEasyGetInfo = (LPFN_REAL_CURLEASYGETINFO)dlsym(RTLD_NEXT, "curl_easy_getinfo");

	lpfnCurlEasyGetInfo(lpCurl, CURLINFO_PRIVATE, &lpcpiData);

	if (__Adt4cRuntimeDebug)
	{ fprintf(stderr, "curl_easy_cleanup() : %p / %p\n", lpCurl, lpcpiData->lpCurl); } 

	if (NULL != lpcpiData->lpszUrl)
	{ free(lpcpiData->lpszUrl); }

	free(lpcpiData);

	return lpfnCurlEasyCleanUp(lpCurl);
}