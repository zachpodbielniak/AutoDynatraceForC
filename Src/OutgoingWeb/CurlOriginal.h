#ifndef ADT4C_CURLORIGINAL_H
#define ADT4C_CURLORIGINAL_H


#include <curl/curl.h>
#include "../Prereqs.h"




typedef CURL *(*LPFN_REAL_CURLEASYINIT)(
	void
);




typedef CURLcode (*LPFN_REAL_CURLEASYPERFORM)(
	CURL 		*lpCurl
);




typedef CURLcode (*LPFN_REAL_CURLEASYCLEANUP)(
	CURL 		*lpCurl
);




typedef CURLcode (*LPFN_REAL_CURLEASYSETOPT)(
	CURL		*lpCurl, 
	CURLoption 	option, 
	...
);




typedef CURLcode (*LPFN_REAL_CURLEASYGETINFO)(
	CURL 		*lpCurl,
	CURLINFO	ciInfo,
	...
);




typedef struct curl_slist *(*LPFN_REAL_CURLSLISTAPPEND)(
	struct curl_slist 	*cslData,
	char			*csData
);




typedef size_t (*LPFN_REAL_CURLHEADERFUNCTIONCALLBACK)(
	char 			*lpszBuffer,
	size_t 			szSize,
	size_t 			szItems,
	void			*lpUserData
);




typedef struct __CURL_PRIVATE_INFO
{
	onesdk_handle_t		osdkhCurl;
	CURL			*lpCurl;
	char 			*lpszUrl;
	struct curl_slist	*cslHeaders;

	LPFN_REAL_CURLHEADERFUNCTIONCALLBACK	lpfnHeaderCallBack;
	void			*lpHeaderUserData;

	void			*lpTruePrivateInfo; /* Normal from CURLOPT_PRIVATE */
} CURL_PRIVATE_INFO, *LPCURL_PRIVATE_INFO;







#endif