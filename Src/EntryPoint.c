#include "Prereqs.h"
#include "Runtime.h"




void
__attribute__((constructor))
__Adt4cConstructor(
	void
){
	char *x;

	x = getenv("ADT4C_DEBUG");
	if (
		x &&
		(0 == strcmp(x, "TRUE") ||
		0 == strcmp(x, "true"))
	){ __Adt4cRuntimeDebug = 1; }

	if (__Adt4cRuntimeDebug)
	{
		fprintf(stderr, "\n");
		fprintf(stderr, "================================\n");
		fprintf(stderr, "AutoDynatraceForC Loaded\n");
		fprintf(stderr, "================================\n\n");
	}

	return;
}




void
__attribute__((destructor))
__Adt4cDestructor(
	void
){
	if (__Adt4cRuntimeDebug)
	{
		fprintf(stderr, "\n");
		fprintf(stderr, "================================\n");
		fprintf(stderr, "AutoDynatraceForC Closing Out\n");
		fprintf(stderr, "================================\n");
	}
	return;
}

