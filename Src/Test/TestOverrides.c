#include "../Prereqs.h"
#include "TestOriginal.h"




void 
__Adt4cTestCustomService(
	void
){
	LPFN_ADT4C_TEST_PROC lpfnTest;
	onesdk_handle_t osdkhService;

	printf("Entering Override of Adt4cTestCustomService()\n");

	lpfnTest = (LPFN_ADT4C_TEST_PROC)dlsym(RTLD_NEXT, "__Adt4cTestCustomService");

	osdkhService = onesdk_customservicetracer_create(
		onesdk_asciistr("Adt4cTestMethod"),
		onesdk_asciistr("Adt4cTestService")
	);

	if (ONESDK_INVALID_HANDLE != osdkhService)
	{ onesdk_tracer_start(osdkhService); }

	if (NULL != lpfnTest)
	{ lpfnTest(); }

	if (ONESDK_INVALID_HANDLE != osdkhService)
	{ onesdk_tracer_end(osdkhService); }

	return;
}