#ifndef ADT4C_MICROHTTPDORIGINAL_H
#define ADT4C_MICROHTTPDORIGINAL_H


#include "../Prereqs.h"



#ifndef __CODE_ENV
#define MHD_OPTION_END		0
struct MHD_Daemon;
struct MHD_Connection;
struct MHD_Response;
struct MHD_PostProcessor;




enum MHD_Result 
{
	MHD_NO = 0,
	MHD_YES = 1
};




enum MHD_ValueKind
{
	MHD_RESPONSE_HEADER_KIND = 0,
	MHD_HEADER_KIND = 1,
	MHD_COOKIE_KIND = 2,
	MHD_POSTDATA_KIND = 4,
	MHD_GET_ARGUMENT_KIND = 8,
	MHD_FOOTER_KIND = 16
};
#endif



typedef enum MHD_Result (*MHD_AcceptPolicyCallback)(
	void				*lpUserData,
	const struct sockaddr 		*addr,
	socklen_t 			addrlen
);




typedef enum MHD_Result (*MHD_AccessHandlerCallback)(
	void 				*lpUserData,
	struct MHD_Connection 		*connection,
	const char 			*lpcszUrl,
	const char 			*lpcszMethod,
	const char 			*lpcszVersion,
	const char 			*lpcszUploadData,
	size_t 				*szUploadDataSize,
	void 				**dlpszConCls
);




typedef struct MHD_Daemon *(*LPFN_REAL_MHDSTARTDAEMON)(
	unsigned int			uiFlags,
	uint16_t			usPort,
	MHD_AcceptPolicyCallback	lpfnApc,
	void 				*lpApcData,
	MHD_AccessHandlerCallback	lpfnHandler,
	void 				*lpHandlerData,
	...
);



typedef struct __MHD_INFO
{
	struct MHD_Daemon			*mhdState;
	MHD_AcceptPolicyCallback		lpfnApc;
	void					*lpHandlerData;
	MHD_AccessHandlerCallback		lpfnHandler;
	void 					*lpApcData;

	onesdk_webapplicationinfo_handle_t	osdkwihApp;
} MHD_INFO, *LPMHD_INFO;



#endif