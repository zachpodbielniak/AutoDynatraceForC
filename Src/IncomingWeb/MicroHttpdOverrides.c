#include "MicroHttpdOriginal.h"

static
int 
__Adt4cRequestHeaders(
	void 				*cls, 
	enum MHD_ValueKind 		kind, 
	const char 			*lpszKey,
	const char 			*lpszValue
){
	kind = kind;
	onesdk_handle_t osdkhRequest;

	osdkhRequest = (onesdk_handle_t)cls;
	onesdk_incomingwebrequesttracer_add_request_header(
		osdkhRequest,
		onesdk_asciistr(lpszKey),
		onesdk_asciistr(lpszValue)
	);

	#ifdef __DEBUG__
	fprintf(stderr, "%s: %s\n", lpszKey, lpszValue);
	#endif

	return 0;
}




static
enum MHD_Result
__Adt4cMicroHttpdHandler(
	void 				*lpUserData,
	struct MHD_Connection 		*connection,
	const char 			*lpcszUrl,
	const char 			*lpcszMethod,
	const char 			*lpcszVersion,
	const char 			*lpcszUploadData,
	size_t 				*szUploadDataSize,
	void 				**dlpszConCls
){
	LPMHD_INFO lpmhdiData;
	enum MHD_Result mhdrRet;
	onesdk_handle_t osdkhRequest;

	lpmhdiData = lpUserData;
	osdkhRequest = onesdk_incomingwebrequesttracer_create(
		lpmhdiData->osdkwihApp,
		onesdk_asciistr(lpcszUrl),
		onesdk_asciistr(lpcszMethod)
	);

	MHD_get_connection_values(
		connection,
		MHD_HEADER_KIND,
		__Adt4cRequestHeaders,
		osdkhRequest
	);

	onesdk_tracer_start(osdkhRequest);

	mhdrRet = lpmhdiData->lpfnHandler(
		lpmhdiData->lpHandlerData,
		connection,
		lpcszUrl,
		lpcszMethod,
		lpcszVersion,
		lpcszUploadData,
		szUploadDataSize,
		dlpszConCls
	);

	onesdk_tracer_end(osdkhRequest);

	return mhdrRet;
}



struct MHD_Daemon *
MHD_start_daemon(
	unsigned int			uiFlags,
	uint16_t			usPort,
	MHD_AcceptPolicyCallback	lpfnApc,
	void 				*lpApcData,
	MHD_AccessHandlerCallback	lpfnHandler,
	void 				*lpHandlerData,
	va_list 			vlData
){
	LPMHD_INFO lpmhdiData;
	LPFN_REAL_MHDSTARTDAEMON lpfnRealMhdStartDaemon;
	struct MHD_Daemon *mhddRet;

	lpfnRealMhdStartDaemon = (LPFN_REAL_MHDSTARTDAEMON)dlsym(RTLD_NEXT, "MHD_start_daemon");
	lpmhdiData = malloc(sizeof(MHD_INFO));

	lpmhdiData->lpApcData = lpApcData;
	lpmhdiData->lpHandlerData = lpHandlerData;
	lpmhdiData->lpfnApc = lpfnApc;
	lpmhdiData->lpfnHandler = lpfnHandler;
	lpmhdiData->osdkwihApp = onesdk_webapplicationinfo_create(
		onesdk_asciistr("MicroHttpd"),
		onesdk_asciistr("MicroHttpd"),
		onesdk_asciistr("/")
	);


	mhddRet = lpfnRealMhdStartDaemon(
		uiFlags,
		usPort,
		lpfnApc,
		lpApcData,
		__Adt4cMicroHttpdHandler,
		lpmhdiData,
		vlData
	);
	va_end(vlData);

	return mhddRet;
}